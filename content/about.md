---
title: "About Me"
date: 2018-01-01T00:00:00-05:00
hide_date: true
---

I'm Nick, a software developer working in the healthcare industry. In my free time I like to focus on Korean, stenography, music, and my many other hobbies.

{{< social_links >}}
